module Main (main) where
import Test.Hspec
import Globber

main :: IO ()
main = hspec $ describe "Testing Globber" $ do
  describe "empty pattern" $ do
    it "matches empty string" $
      matchGlob "" "" `shouldBe` True
    it "shouldn't match non-empty string" $
      matchGlob "" "string" `shouldBe` False
  describe "single character pattern" $ do
    it "matches a single literal" $
      matchGlob "a" "a" `shouldBe` True
    it "doesn't match the wrong literal" $
      matchGlob "a" "b" `shouldBe` False
    it "respects ?" $
      matchGlob "?" "x" `shouldBe` True
    it "respects * for zero character match" $
      matchGlob "*" "" `shouldBe` True
    it "respects * for single character match" $
      matchGlob "*" "y" `shouldBe` True
    it "respects * for multiple character match" $
      matchGlob "*" "acbde" `shouldBe` True
  describe "multiple character pattern" $ do
    it "lets you escpae ?" $
      matchGlob "\\?" "?" `shouldBe` True
    it "lets you escape *" $
      matchGlob "\\*" "*" `shouldBe` True
    it "matches literals" $
      matchGlob "abcde" "abcde" `shouldBe` True
    it "rejects non-mathcing literals" $
      matchGlob "abcde" "abcdf" `shouldBe` False
    it "supports ?" $
      matchGlob "ab?de" "abcde" `shouldBe` True
    it "supports * at end of string" $
      matchGlob "ab*" "abcde" `shouldBe` True
    it "supports * in the middle of string" $
      matchGlob "ab*e" "abcde" `shouldBe` True
    it "supports * in the middle of a string followed by an escape" $
      matchGlob "a*\\?" "abcdefghijk\\?" `shouldBe` True
