module Globber (matchGlob) where

type GlobPattern = String
matchGlob :: GlobPattern -> String -> Bool
matchGlob [] [] = True
matchGlob "*" [] = True
matchGlob _ [] = False
matchGlob [] xs = False
matchGlob (p:[]) (x:[])
  | p == '?' = True
  | p == '*' = True
  | otherwise = p == x
matchGlob (p:[]) (x:xs)
  | p == '*' = True
  | otherwise = False
matchGlob (p:ps) (x:xs)
  | p == '\\' = (head ps == x) && (matchGlob (tail ps) xs)
  | p == '*' = any (matchGlob ps) [drop n (x:xs) | n <- [0..(length (x:xs))]]
  | otherwise = (matchGlob [p] [x]) && (matchGlob ps xs)


